from flask import Flask, request, redirect, render_template, flash, jsonify, Response, url_for
from bson import ObjectId
from pymongo import MongoClient, errors
from werkzeug.exceptions import BadRequestKeyError

from datetime import datetime
from os import urandom
import re

app = Flask(__name__)
client = MongoClient("mongodb://localhost:27017")
db = client.address_book

def extract_value(field):
    try:
        return request.form[field]
    except KeyError:
        return None

def valid_email(email):  
    email_regex = re.compile(
        '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'
        )
    if not email_regex.match(str(email)):
        return False
    return True

def valid_phone_number(phone_number):
    phone_number_regex = re.compile('\d{10}')
    if not phone_number_regex.match(phone_number):
        return False
    return True

@app.route('/', methods=['GET'])
def index():
    q = request.args.get('q')
    query = {}
    if q and valid_email(q):
        query['email'] = q
    elif q and valid_phone_number(q):
        query['phone_number'] = q
    elif q:
        query['name'] = str(q)
    addresses = db.phone_book.find(query)
    book = []
    for addr in addresses:
        book.append(addr)
    return render_template('address-book.html', contacts=book)

@app.route('/new', methods=['GET'])
@app.route('/edit/<contact_id>', methods=['GET'])
def add_contact(contact_id=None):
    address=None
    if contact_id:
        address = db.phone_book.find_one({'_id': ObjectId(contact_id)})
    if contact_id and not address:
        flash("Unable to handle your request.")
        return redirect(url_for('add_contact'))
    req=request.form
    return render_template('operations.html', req=req, address=address)

@app.route('/new/', methods=['POST'])
def new_contact(contact_id=None):
    name = extract_value('name')
    email = extract_value('email')
    phone_number = extract_value('phone_number')
    create_at = datetime.now()
    update_at = datetime.now()
    # sanitizing values
    email = email if valid_email(email) else None
    phone_number = phone_number if valid_phone_number(str(phone_number)) \
    else None
    address = {'name': name, 'email': email, 'phone_number': phone_number,\
     'created_at': create_at, 'updated_at':update_at}
    if name and (email or phone_number):    
        db.phone_book.insert_one(address)
        flash("Contact book updated succesfully")
        return redirect(url_for('index'))
    return redirect(url_for('add_contact'))

@app.route('/edit/<contact_id>', methods=['POST'])
def save_contact(contact_id):
    contact_id = ObjectId(contact_id)
    if db.phone_book.find_one({'_id': contact_id}) is None:
        return Response("Invalid id", 304)
    name = extract_value('name')
    email = extract_value('email')
    phone_number = extract_value('phone_number')
    update_at = datetime.now()
    email = email if valid_email(email) else None
    phone_number = phone_number if valid_phone_number(str(phone_number)) else None
    try:
        db.phone_book.update_one({'_id': ObjectId(contact_id)}, {'$set': {"name": name, "email": email, "phone_number": phone_number, 'updated_at': update_at}})
        
    except errors.CursorNotFound:
        return Response("Invalid id", 304)
    flash("Contact book updated succesfully")
    return redirect(url_for('index'))

@app.route('/delete/<contact_id>', methods=['POST'])
def delete_contact(contact_id):
    if db.phone_book.find_one({'_id': ObjectId(contact_id)  }) is None:
        flash("Unable to handle your request")
        return redirect(url_for('index'))
    db.phone_book.remove({"_id":ObjectId(contact_id)})
    flash("successfully deleted")
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.secret_key = urandom(24)
    app.config['SESSION_TYPE'] = 'filesystem'
    # sess.init_app(app)
    app.run(port=3000, debug=True)

